import java.sql.*;

public class Procedure {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/chetan";

    static final String USER = "root";
    static final String PASS = "12345678";

    public static void main(String args[]){

        Connection cn = null;
        CallableStatement procedure = null;
        try {
            Class.forName(JDBC_DRIVER);
            cn = DriverManager.getConnection(DB_URL, USER, PASS);
            procedure = cn.prepareCall("{call getProducts()}");
            ResultSet rs = procedure.executeQuery();
            System.out.println("ID \t Name \tPublisher \tMadeIn \tPrice");
            while (rs.next()){
                int id= rs.getInt("id");
                String name = rs.getString("name");
                String pub = rs.getString("brand");
                String madein =rs.getString("madein");
                float price = rs.getFloat("price");

                System.out.println(id+"\t"+name+"\t"+pub+"\t"+madein+"\t"+price);
            }
            rs.close();
            procedure.close();
            cn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}

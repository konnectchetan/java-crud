import java.sql.*;

public class Batch {

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/chetan";

    static final String USER = "root";
    static final String PASS = "12345678";

    public static void main(String args[]){
        System.out.println("Test");
        Connection cn = null;
        Statement stmt = null;
        try{
            Class.forName(JDBC_DRIVER);
            cn = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = cn.createStatement();
            System.out.println("Connection Established");
            cn.setAutoCommit(false);

            String sql = "insert into product (name,brand,madein,price) values ('ML Book','KDP','India',900)";
            stmt.addBatch(sql);

            sql = "insert into product  (name,brand,madein,price) values ('Learning DataScience','KDP','India',500)";
            stmt.addBatch(sql);

            sql ="insert into product  (name,brand,madein,price) values ('Argumented Reality Book','KDP','India',750)";
            stmt.addBatch(sql);

            sql ="insert into product  (name,brand,madein,price) values ('Self Learning','KDP','India',340)";
            stmt.addBatch(sql);
            System.out.println("Executing Batch");
            stmt.executeBatch();
            //int[] count = stmt.executeBatch();
            System.out.println(" Batch Executed");
            cn.commit();

            //System.out.println(" "+count);

            sql = "select * from product" ;
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("ID \t Name \tPublisher \tMadeIn \tPrice");
            while (rs.next()){
                int id= rs.getInt("id");
                String name = rs.getString("name");
                String pub = rs.getString("brand");
                String madein =rs.getString("madein");
                float price = rs.getFloat("price");

                System.out.println(id+"\t"+name+"\t"+pub+"\t"+madein+"\t"+price);
            }
            rs.close();
            stmt.close();
            cn.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}

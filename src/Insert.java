import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Insert {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/doctor";

    static final String USER = "root";
    static final String PASS = "12345678";

    public static void main(String args[]){
        Connection con = null;
        Statement stmt = null;
        try{
            Class.forName(JDBC_DRIVER);
            //Creating Db Connection
            con = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = con.createStatement();
            String sql = "insert into Patient values (53,'Chetan Talwar','Chandigarh','2332',23,2)";
            stmt.executeUpdate(sql);

            stmt.close();
            con.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}

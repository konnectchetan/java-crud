import java.sql.*;

public class Select {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/doctor";

    static final String USER = "root";
    static final String PASS = "12345678";

    public static void main(String args[]){
        Connection con = null;
        Statement stmt = null;
        try{
            Class.forName(JDBC_DRIVER);
            //Creating Db Connection
            con = DriverManager.getConnection(DB_URL,USER,PASS);

            stmt = con.createStatement();
            String sql = "select * from Patient";

            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                String name = rs.getString(2);
                String address = rs.getString("Address");

                System.out.println("Name - "+name + "   "+address);
            }
            rs.close();
            stmt.close();
            con.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}


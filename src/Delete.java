import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Delete {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/doctor";

    static final String USER = "root";
    static final String PASS = "12345678";

    public static void main(String args[]){
        Connection con = null;
        Statement stmt = null;
        try{
            Class.forName(JDBC_DRIVER);
            //Creating Db Connection
            con = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = con.createStatement();
            String sql = "delete from Patient where ssn=453";
            stmt.executeUpdate(sql);


            sql = "select * from patient";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getString("Name")+"   "+rs.getString("Address"));
            }

            stmt.close();
            con.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
